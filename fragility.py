from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
import scipy
import numpy as np
import matplotlib.pyplot as plt

def edp_regression(  im: list, 
                     edp : list, 
                     plot = True ,
                     ):

    """
    This function performs linear regression.

    INPUTS:
    IM           IM list
    EDPs         EDP list
    """       
    
  # Create linear regression object
    regr = linear_model.LinearRegression()

    logim = np.log(np.array(im))
    logedp = np.log(np.array(edp))
    xv = logim[:, np.newaxis]
    y = logedp[:, np.newaxis]

    # Fit data
    regr.fit(xv, y)
    c = regr.predict(xv)
    # The coefficients
    print("Coefficients: ", regr.coef_)
    print("Intercept: ", regr.intercept_)
    # The mean squared error
    print("Mean squared error: %.2f" % mean_squared_error(y, c)) 
    print("sigma: %.2f" % np.sqrt(mean_squared_error(y, c)))     
    # The coefficient of determination: 1 is perfect prediction
    print("Coefficient of determination: %.2f" % r2_score(y, c))


    if plot :
        plt.figure('Regression')
        plt.xticks()
        plt.yticks()
        plt.grid()

        plt.scatter(xv, y)
        x0 = np.linspace(np.min(xv), np.max(xv), 100)
        x0list = x0[:, np.newaxis]
        clist = regr.predict(x0list)
        plt.plot(x0list, clist,  color="blue", linewidth=3)
        yf = xv * regr.coef_ + regr.intercept_
        plt.plot(xv, yf, '--', color="red", linewidth=3)


    sigma = np.sqrt(mean_squared_error(y, c))

    return regr, sigma


def fragility_regression(
                     ims: list, 
                     edps: list, 
                     threshold: float, ):
    
    
        """
    This function estimates the parameters of a lognormal fragility curve from IM-EDP
    data using linear regression. The method is from:
    
    Zentner, I. et al. (2016). “Fragility analysis methods: Review of existing approaches and
    application.” Nuclear Engineerinf Design, 323(5).

    INPUTS:
    IM           IM list
    EDPs         EDP list

    OUTPUTS:
    Am         median of fragility function
    beta      lognormal standard deviation of fragility function
        """   
    

        reg, sigm = edp_regression(ims, edps)

        coef_c = reg.coef_
        coef_b = np.exp(reg.intercept_)

        am = np.exp( np.log(threshold/coef_b) / coef_c)
        beta = sigm / coef_c

        return am, beta
    
def compute_frag(xlist, Am, beta):
    return scipy.stats.norm.cdf(np.log(xlist / Am) / beta)   
    
